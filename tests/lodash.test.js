const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect;

// test case1
test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

// test case2
test('Should chunk an array', () => {
    const array = ['a', 'b', 'c', 'd'];
    let chunkedArr = _.chunk(array, 2);
    expect(chunkedArr).to.eql([['a', 'b'], ['c', 'd']]);
})

// test case3
test("Should Filter array by difference", () => {
    const arraySource = [2, 1];
    const arrayTarget = [2, 3];
    const differenceArray = _.difference(arraySource, arrayTarget);
    expect(differenceArray).to.eql([1]);
})

// test case4
test("Should drop number of elements", () => {
    const array = [1, 2, 3];
    const droppedArray = _.drop(array, 2);
    expect(droppedArray).to.eql([3]);
})

// test case5
test("Fills elements of array with value from start up to , but not including , end", () => {
    const array = [4, 6, 8, 10];
    const filledArray = _.fill(array, '*', 1, 3);
    expect(filledArray).to.eql([4, '*', '*', 10]);
})

// test case6
test("Find index of the found element", () => {
    const users = [
        { 'name': 'barney', 'active': false },
        { 'name': 'fred', 'active': false },
        { 'name': 'pebbles', 'active': true }
    ];
    const findedUsers = _.findIndex(users,(user) => 
        user.name == 'fred'
    );
    expect(findedUsers).to.equal(1);
})

// test case7
test("Find the head of the array", () => {
    const array = [1,2,3];
    const headedArray = _.head(array);
    expect(headedArray).to.equal(1);
})

// test case 8
test("Find the intersaction between two arrays", () => {
    const sourceArray = [2,1];
    const targetArray = [2,3];
    const interSectedArray = _.intersection(sourceArray,targetArray);
    expect(interSectedArray).to.eql([2]);
})

// test case 8
test("Gets all but the last element of array", () => {
    const array = [1,2,3];
    const intialArray = _.initial(array);
    expect(intialArray).to.eql([1,2]);
})

// test case 9
test("Converts all elements in array into a string separated by separator", () => {
    const array = ['a','b','c'];
    const joinedArray = _.join(array,'~');
    expect(joinedArray).to.equal("a~b~c")
})

// test case 10
test("Removes all elements from array that predicate returns truthy for and return an array of the removed elements", () => {
    const array = [1,2,3,4];
    const removedArray = _.remove(array, (element) => 
        element % 2 == 0 
    )
    expect(array).to.eql([1,3]);
})

// test case 11
test("Creates an array of unique values that is symmetric difference of the given arrays", () => {
    const sourceArray = [2,1];
    const targetArray = [2,3];
    const xoredArray = _.xorBy(sourceArray,targetArray);
    expect(xoredArray).to.eql([1,3]);
})