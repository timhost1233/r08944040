
let sorting = (array) => {
    // ascending order
    return array.sort();
}

let compare = (a, b) => {
    return (a['PM2.5']).localeCompare(b['PM2.5']);
}

let average = (nums) => {
    let sum = 0;
    for(let index = 0;index < nums.length;index++) {
        sum += nums[index];
    }
    // console.log("this is average");
    // console.log(nums);
    let result = sum / nums.length;
    result = Math.round(result * 100 + Number.EPSILON) / 100;
	return result;
}


module.exports = {
    sorting,
    compare,
    average
}